#include "DBAsteriskConfig.h"

DBAsteriskConfig::DBAsteriskConfig()
{
	
}

DBAsteriskConfig::~DBAsteriskConfig()
{
	
}

void DBAsteriskConfig::DBConnect()
{
	conn = PQconnectdb("dbname=AsteriskConfig host=localhost user=postgres password=postgres");

	if (PQstatus(conn) == CONNECTION_BAD) {
		puts("We were unable to connect to the database");
		//exit(0);
	}
}

void DBAsteriskConfig::DBDisconnect()
{
	PQclear(res);

	PQfinish(conn);
}

std::string DBAsteriskConfig::getScriptLua( const std::string & variable, const std::string & site )
{
	std::string str;
	str.reserve(10000);
	std::stringstream ss(str);	
	
	ss << boost::format("select scriptlua from public.config where variable = '%s' and site = '%s'") % variable % site;
	
	printf("======== query ... ====\r\n");
	printf(ss.str().c_str());
	printf("\r\n=======================\r\n\r\n");
	
	res = PQexec(conn, ss.str().c_str() );
	
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		puts("We did not get any data!");
		//exit(0);
	}

	rec_count = PQntuples(res);
	
	//printf("We received %d records.\n", rec_count);
	
	ss.str(std::string());
	if ( rec_count > 0 )		
	{
		ss << boost::format("%s") % PQgetvalue(res, 0, 0);
		//printf(ss.str().c_str() );
	}
		
	return ss.str();	
}

std::string DBAsteriskConfig::getScriptLua( const std::string & variable, const std::string & site , const std::string & ura)
{
	std::string str;
	str.reserve(10000);
	std::stringstream ss(str);	
	
	ss << boost::format("select scriptlua from public.config where variable = '%s' and site = '%s' and ura = '%s'") % variable % site % ura;
	
	printf("======== query ... ====\r\n");
	printf(ss.str().c_str());
	printf("\r\n=======================\r\n\r\n");
	
	res = PQexec(conn, ss.str().c_str() );
	
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		puts("We did not get any data!");
		//exit(0);
	}

	rec_count = PQntuples(res);
	
	//printf("We received %d records.\n", rec_count);
	
	ss.str(std::string());
	if ( rec_count > 0 )		
	{
		ss << boost::format("%s") % PQgetvalue(res, 0, 0);
		//printf(ss.str().c_str() );
	}
		
	return ss.str();	
}