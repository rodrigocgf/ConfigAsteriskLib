#ifndef _CONFIGCONTAINER_
#define _CONFIGCONTAINER_

#include <string>

class ConfigContainer
{
public:
	ConfigContainer() {};
	ConfigContainer(const std::string & _variable, const std::string & _site , const std::string & _ura, const std::string & _scritptLua) :
		variable(_variable), site(_site) , ura( _ura ) , scriptLua ( _scritptLua)
		{};
	~ConfigContainer() {};
	
	std::string variable;
	std::string site;
	std::string ura;
	std::string scriptLua;
};

#endif