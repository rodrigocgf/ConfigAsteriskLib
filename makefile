BOOST_ROOT = /opt/apps/boost_1_66_0
ARI_ROOT = /home/rodrigo/projects/c++
POSTGRES = /usr/local/pgsql
  
LIB=-L$(BOOST_ROOT)/stage/lib \
	-L$(BOOST_ROOT)/stage/lib/libboost_thread.a \
	-L$(BOOST_ROOT)/stage/lib/libboost_system.a \
	-L$(BOOST_ROOT)/stage/lib/libboost_filesystem.a \
	-L$(BOOST_ROOT)/stage/lib/libboost_timer.a \
	-L$(BOOST_ROOT)/stage/lib/libboost_exception.a \
	-L$(BOOST_ROOT)/stage/lib/libboost_regex.a \
	-L$(POSTGRES)/lib -lpq
	

INC=-I$(BOOST_ROOT) -I$(LUA_ROOT) -I$(ARI_ROOT) -I$(POSTGRES)/include


CFLAGS=gcc  -m64 -Wall -ansi -Werror -pthread -stdlib=libc++ -O2 -fPIC
CPPFLAGS=g++ -m64 -Wall -std=c++1y  -pthread -g -O -shared -fPIC

# the compiler: gcc for C program, define as g++ for C++
CC = g++

# the build target executable:
TARGET = ConfigAsterisk.so

all: $(TARGET)

#$(TARGET): $(TARGET).cpp
#	$(CPPFLAGS) -o $(TARGET) $(TARGET).cpp $(LIB) $(INC)

#$(TARGET): ConfigAsterisk.o DBAsteriskConfig.o
#	$(CPPFLAGS) -o $(TARGET) ConfigAsterisk.o DBAsteriskConfig.o $(LIB) $(INC)
	
$(TARGET): ConfigAsterisk.o Controller.o DBAsteriskConfig.o
	$(CPPFLAGS) -o $(TARGET) ConfigAsterisk.o Controller.o DBAsteriskConfig.o $(LIB) $(INC)

ConfigAsterisk.o : ConfigAsterisk.cpp 
	$(CPPFLAGS) -g -c ConfigAsterisk.cpp $(LIB) $(INC)

Controller.o : Controller.cpp 
	$(CPPFLAGS) -g -c Controller.cpp $(LIB) $(INC)	
	
DBAsteriskConfig.o : DBAsteriskConfig.cpp 
	$(CPPFLAGS) -g -c DBAsteriskConfig.cpp $(LIB) $(INC)	
clean:
	$(RM) $(TARGET)
	rm -rf *.o