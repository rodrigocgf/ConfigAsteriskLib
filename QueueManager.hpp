#pragma once

#include <deque>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>

template<typename T>
class QueueManager 
{
public:
	typedef boost::shared_ptr<T>	DataTypePtr;
	typedef std::deque<DataTypePtr> QueueType;
	
	QueueManager()
	:m_stop(false)
	{
	}

	void enqueue(DataTypePtr & data)
	{
		{
			boost::mutex::scoped_lock lk( m_mutex );
			m_queue.push_back(data);
		}
		m_cond.notify_one();
	}
	
	void stop()
	{
		m_stop = true;
		m_cond.notify_all();
	}

	void operator()()
	{
		while ( !m_stop )
		{
			boost::mutex::scoped_lock lk(  m_mutex );
			while ( m_queue.empty() && !m_stop )
				m_cond.wait( lk );
			
			if ( !m_stop)
			{
				ASSERT( lk.owns_lock() );
				ASSERT( m_queue.size() );

				DataTypePtr spExec = m_queue.front();
				//ASSERT( spExec.get() );
				m_queue.pop_front();
				lk.unlock();
				(*spExec)();
			}
		}
	}
private:
	QueueType			m_queue;
	boost::mutex		m_mutex;
	boost::condition	m_cond;
	volatile bool		m_stop;
};
