#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/shared_ptr.hpp>

#include "ConfigContainer.hpp"
#include "DBAsteriskConfig.h"

using boost::multi_index_container;
using namespace boost::multi_index;

struct ByVariableSiteUra{};

typedef multi_index_container
<
	boost::shared_ptr<ConfigContainer>,
	indexed_by<		
		ordered_unique< 
			tag<ByVariableSiteUra>,
			composite_key<
				boost::shared_ptr<ConfigContainer>,
				member< ConfigContainer, std::string, &ConfigContainer::variable>,
				member< ConfigContainer, std::string, &ConfigContainer::site>,
				member< ConfigContainer, std::string, &ConfigContainer::ura>
			>
		>		
	>
> DictConfig;

class Controller
{	
public:
	Controller();
	~Controller();
	
	void ProcessRequest(const char * psz_variable , char * psz_ScriptLua);
	
	bool LookupContainer(const std::string & strVariable , const std::string & strSite , const std::string & strUra , std::string & strScriptLua);
	void InsertContainer( const std::string & strVariable , const std::string & strSite , const std::string & strUra , const std::string & strScriptLua);
	
	void RecoverAtDatabase(const std::string & strVariable , const std::string & strSite , const std::string & strUra , std::string & strScriptLua);
	
	DictConfig m_DictConfig;
	boost::shared_ptr<DBAsteriskConfig> m_dbAsteriskConfigPtr;
};

#endif
