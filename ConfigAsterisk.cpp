#include "ConfigAsterisk.h"
#include <boost/shared_ptr.hpp>
#include <boost/format.hpp>
#include "Controller.h"

char * getLuaScript(const char * psz_variable )
{	
	char * szScriptLua = new char[20000]; // must allocate in Heap memory, not in Stack memory !
	
	boost::shared_ptr<Controller> controllerPtr;
	controllerPtr.reset(new Controller());
	
	memset(szScriptLua,0x00,sizeof(szScriptLua));
	controllerPtr->ProcessRequest(psz_variable, szScriptLua);	
	
	return szScriptLua;
}
