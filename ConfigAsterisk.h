#ifndef CTEST_H
#define CTEST_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

	char * getLuaScript(const char * psz_variable);

#ifdef __cplusplus
}

#endif
#endif