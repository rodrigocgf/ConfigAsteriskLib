#include <stdio.h>
#include <postgresql/libpq-fe.h>
#include <string>
#include <sstream>
#include <boost/format.hpp>

class DBAsteriskConfig
{
	PGconn          *conn;
	PGresult        *res;
	int             rec_count;
	int             row;
	int             col;
	
	
public:	
	DBAsteriskConfig();
	~DBAsteriskConfig();
	
	void DBConnect();
	void DBDisconnect();
	
	std::string getScriptLua( const std::string & variable, const std::string & site );
	std::string getScriptLua( const std::string & variable, const std::string & site , const std::string & ura);
};