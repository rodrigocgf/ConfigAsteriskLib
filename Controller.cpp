#include "Controller.h"


Controller::Controller()
{
	
}

Controller::~Controller()
{
	
}

void Controller::ProcessRequest(const char * psz_variable , char * psz_ScriptLua)
{
	std::string strVariable;
	std::string strSite;
	std::string strUra;
	std::string strScriptLua;

	strSite.assign("SPVV");
	
	if ( psz_variable != NULL )
		strVariable.assign(psz_variable);
	
	if ( LookupContainer(strVariable , strSite , strUra , strScriptLua ) )
	{	
		printf("FOUND AT CONTAINER\r\n");
		strcpy( psz_ScriptLua , strScriptLua.c_str() );
	} else {
		// Recover at Database
		printf("GOING TO RECOVER AT DATABASE\r\n");
		RecoverAtDatabase(strVariable , strSite , strUra , strScriptLua );
		
		if ( strScriptLua.length() > 0 )
		{
			printf("GOING TO INSERT AT CONTAINER\r\n");
			InsertContainer( strVariable , strSite , strUra , strScriptLua );
		}
		
		strcpy( psz_ScriptLua , strScriptLua.c_str() );
	}
}

void Controller::RecoverAtDatabase(const std::string & strVariable , const std::string & strSite , const std::string & strUra , std::string & strScriptLua)
{
	m_dbAsteriskConfigPtr.reset(new DBAsteriskConfig());
	m_dbAsteriskConfigPtr->DBConnect();
	//strScriptLua = m_dbAsteriskConfigPtr->getScriptLua( strVariable , strSite , strUra);
	strScriptLua = m_dbAsteriskConfigPtr->getScriptLua( strVariable , strSite );
	
	printf("SCRIPT LUA FOR variable %s / site %s / ura %s : \r\n", strVariable.c_str(), strSite.c_str(), strUra.c_str() );
	printf("................................................\r\n");
	printf(strScriptLua.c_str());	
	printf("\r\n................................................\r\n");
	printf("\r\n");	
	
	m_dbAsteriskConfigPtr->DBDisconnect();
}

bool Controller::LookupContainer(const std::string & strVariable , const std::string & strSite , const std::string & strUra , std::string & strScriptLua)
{
	bool bFound = false;	
	
	auto ConfigContainerItr = m_DictConfig.get<ByVariableSiteUra>().find(std::make_tuple(strVariable , strSite , strUra ));			
	if ( ConfigContainerItr != m_DictConfig.get<ByVariableSiteUra>().end() )
	{
		boost::shared_ptr<ConfigContainer> ConfigContainerPtr = (*ConfigContainerItr);
		strScriptLua = ConfigContainerPtr->scriptLua;
		
		bFound = true;
	}
	
	return bFound;	
}

void Controller::InsertContainer( const std::string & strVariable , const std::string & strSite , const std::string & strUra , const std::string & strScriptLua)
{
	
	auto ConfigContainerItr = m_DictConfig.get<ByVariableSiteUra>().find(std::make_tuple(strVariable , strSite , strUra ));			
	if ( ConfigContainerItr == m_DictConfig.get<ByVariableSiteUra>().end() )
	{
		boost::shared_ptr<ConfigContainer> ConfigContainerPtr( new ConfigContainer( strVariable, strSite, strUra , strScriptLua ) );
		m_DictConfig.insert( ConfigContainerPtr );
	}
}